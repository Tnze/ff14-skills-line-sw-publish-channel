$pluginFile = Get-Item $args[0]

$thisPath = Split-Path -Parent $MyInvocation.MyCommand.Source

New-Object PSObject -Property @{
    latest = $pluginFile.VersionInfo.ProductVersion
} | ConvertTo-Json | Out-File -Encoding ascii (Join-Path $thisPath "versions.json")